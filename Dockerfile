FROM node:18.17.1

RUN apt-get update && \
  apt-get install -y \
  build-essential

RUN apt-get install -y lsb-release 

RUN apt-get install sudo

RUN wget -O- https://apt.releases.hashicorp.com/gpg | \
    gpg --dearmor | \
    sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg

RUN gpg --no-default-keyring \
    --keyring /usr/share/keyrings/hashicorp-archive-keyring.gpg \
    --fingerprint

RUN echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] \
    https://apt.releases.hashicorp.com $(lsb_release -cs) main" | \
    sudo tee /etc/apt/sources.list.d/hashicorp.list

RUN sudo apt update

RUN sudo apt install terraform

RUN node -v && npm -v && terraform --version

